To get this into your own Jupyter Notebook, clone from this repository by clicking on the blue clone button and copying the "Clone with SSH address to clipboard."

Then open your terminal and type "git clone " followed by the SSH address on your clipboard.

Once you've entered this, you should see a file titled Week 1 Final Test Project. Open it, open the testing file, open the Visualization in Python Week 1 Demo Project in your Jupyter Notebook.

From here follow the instructions in the file via your Jupyter Notebook.